'use strict';

var services = angular.module('lampApp.services', ['ngResource']);

var baseUrl = 'http://localhost\\:3000';

services.factory('NoticeFactory', function ($resource) {
    return $resource(baseUrl + '/notice', {}, {
        query: { method: 'GET', params: {} }
    });
});

services.factory('LampsFactory', function ($resource) {
    return $resource(baseUrl + '/lamps', {}, {
        query: { method: 'GET', isArray: true }
    });
});

services.factory('LampFactory', function ($resource) {
    return $resource(baseUrl + '/lamps/:id', {}, {
        show: { method: 'GET' }
    });
});