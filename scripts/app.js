"use strict";

// Declare app level module which depends on views, and components
angular.module('LampApp', [
    'ngResource',
    'ngRoute',
    'lampApp.services',
    'lampApp.controllers'
]).
config(['$routeProvider', function ($routeProvider, $httpProvider) {
    $routeProvider.when('/notice', { templateUrl: 'views/notice.html', controller: 'NoticeCtrl'});
    $routeProvider.when('/list', { templateUrl: 'views/list.html', controller: 'ListCtrl'});
    $routeProvider.otherwise({redirectTo: '/notice'});
}]);