'use strict';

var app = angular.module('lampApp.controllers', []);

// Clear browser cache (in development mode)
//
// http://stackoverflow.com/questions/14718826/angularjs-disable-partial-caching-on-dev-machine
app.run(function ($rootScope, $templateCache) {
    $rootScope.$on('$viewContentLoaded', function () {
        $templateCache.removeAll();
    });
});

app.controller('NoticeCtrl', ['$scope', 'NoticeFactory', function ($scope, NoticeFactory) {
    $scope.message = '간단한 AngularJS 테스트 프로그램입니다. Lamp 상태를 보여줍니다. 아직 Lamp 조작 기능은 구현되어 있지 않습니다.';
}]);

app.controller('ListCtrl', ['$scope', 'LampsFactory', function ($scope, LampsFactory) {
    $scope.lamps = LampsFactory.query();
}]);